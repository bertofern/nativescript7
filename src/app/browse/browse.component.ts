import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { registerElement } from "nativescript-angular/element-registry";
import * as app from "tns-core-modules/application";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);

var gmaps = require("nativescript-google-maps-sdk");

@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})

export class BrowseComponent implements OnInit {
    @ViewChild("MapView", { static: false }) mapView: ElementRef;

    constructor() { }

    ngOnInit(): void { }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onMapReady(event): void {
        console.log("Map Ready");

        var mapView = event.object;
        var marker = new gmaps.Marker();

        marker.position = gmaps.Position.positionFromLatLng(42.268131, 2.960178);
        marker.title = "Figueres";
        marker.snippet = "España";
        marker.color = "#483D8B";
        marker.userData = { index : 1 };
        mapView.addMarker(marker);
    }
}
