import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { RouterExtensions } from "nativescript-angular/router";
import { Noticia } from "../models/noticia.model";
// import { View, Color } from "tns-core-modules/ui/page";
import * as Toast from "nativescript-toast";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { NuevaNoticiaAction } from "../domain/noticias-state.model";
import { GestureEventData } from "tns-core-modules/ui/gestures";
import * as SocialShare from "nativescript-social-share";
import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/core/view";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    // idNoticia: number;
    noticia: Noticia;
    // resultados: Array<Noticia>;
    resultados: Array<string>;
    resultadosNoticias: Array<Noticia>;

    @ViewChild("layout", { static: false }) layout: ElementRef;

    constructor(
        private noticias: NoticiasService, 
        private store: Store<AppState>,
        private routerExtensions: RouterExtensions
    ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.noticiaSugerida)
            .subscribe((data) => {
                const ns = data;
                if (ns != null) {
                    Toast.makeText("Sugerimos leer: " + ns.titulo, "short").show();
                }
            });
    }

    onButtonTap(): void {
        // Datos de muestra
        var n1 = new Noticia(1,'Futbol Barcelona...','Deportes', 'lorem ipsum', 1);
        var n2 = new Noticia(2,'Futbol Sevilla...','Deportes', 'lorem ipsum', 1);
        var n3 = new Noticia(3,'Futbol Lech...','Deportes', 'lorem ipsum', 1);
        var n4 = new Noticia(4,'Futbol Millan...','Deportes', 'lorem ipsum', 1);
        var n5 = new Noticia(5,'Polonia...','Politica', 'lorem ipsum', 1);
        var n6 = new Noticia(6,'España...','Politica', 'lorem ipsum', 1);
        var n7 = new Noticia(7,'Estados Unidos...','Politica', 'lorem ipsum', 1);
        var n8 = new Noticia(8,'PIB en España...','Economia', 'lorem ipsum', 1);
        var n9 = new Noticia(9,'PIB en Polonia...','Economia', 'lorem ipsum', 1);
        var n10 = new Noticia(10,'Descubren...','Ciencia', 'lorem ipsum', 1);

        this.noticias.agregarNoticia(n1);
        this.noticias.agregarNoticia(n2);
        this.noticias.agregarNoticia(n3);
        this.noticias.agregarNoticia(n4);
        this.noticias.agregarNoticia(n5);
        this.noticias.agregarNoticia(n6);
        this.noticias.agregarNoticia(n7);
        this.noticias.agregarNoticia(n8);
        this.noticias.agregarNoticia(n9);
        this.noticias.agregarNoticia(n10);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        this.noticias.noticiaElegida = this.resultadosNoticias[x.index];
        this.store.dispatch(new NuevaNoticiaAction(this.noticias.noticiaElegida));
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    onTap(args: GestureEventData, x: Noticia) {
        console.log("Tap");
        console.log("Object that triggered the event: " + args.object);
        console.log("View that triggered the event: " + args.view);
        console.log("Event name: " + args.eventName);
        console.log(x.favorita);
        if (x.favorita === false) {
            x.favorita = true;
            this.noticias.postFavorita(x);
            Toast.makeText("'" + x.titulo + "' se agregó a Favoritos", "short").show();
        } else {
            x.favorita = false;
            this.noticias.deleteFavorita(x);
            Toast.makeText("'" + x.titulo + "' se quitó de Favoritos", "short").show();
        }
        console.log(x.favorita);
    }

    onNavButton(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        })
    }

    buscarNoticia(n: string) {
        console.dir("buscarNoticia: " + n);
        this.noticias.searchNoticia(n).then((r: any) => {
            console.log("Resultados buscarNoticia: " + JSON.stringify(r));
            this.resultadosNoticias = r;

            // Animation
            const layout = <View>this.layout.nativeElement;
            layout.rotate = 0;
            layout.animate({
                backgroundColor: new Color("blue"),
                duration: 300,
                delay: 150
            }).then(()=> layout.animate({
                backgroundColor: new Color("white"),
                duration: 300,
                delay: 150
            }));
            // END Animation

        }, (e) => {
            console.log("ERROR buscarNoticia: " + e);
            Toast.makeText("Error en la búsqueda").show();
        });
    }

    agregarNoticia(): void {
        if(this.noticias.noticiaCounter > 0) {
            let idNoticia = this.noticias.noticiaCounter;

            this.noticia = new Noticia(
                idNoticia,
                "Noticia " + idNoticia,
                "General",
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                this.noticia.countReview = 1
            );
            
            this.noticias.agregarReviewInicial(this.noticia.id);
            this.noticias.agregarNoticia(this.noticia);
        }
    }
}
