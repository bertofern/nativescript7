import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { Noticia } from "../models/noticia.model";

@Component({
  selector: 'ns-news',
  templateUrl: './news.component.html'
})
export class NewsComponent implements OnInit {

  misNoticias: Array<Noticia> = [];

  constructor(private noticias: NoticiasService) { }

  ngOnInit(): void {
    this.noticias.obtenerNoticias().then((f: any) => {
      this.misNoticias = f;
    })
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }
}
