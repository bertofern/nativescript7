var express = require('express');
var cors = require('cors');
var app = express();

app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log('Server running on port 3000'));

var noticias = [];
app.get('/news', (req, res, next) => {
    res.json(noticias.filter((c) => c.titulo.toLocaleLowerCase().indexOf(req.query.q.toString().toLocaleLowerCase())> -1));
    //res.json(noticias.filter((c) => c.titulo.indexOf(req.query.q.toString().toLowerCase())> -1));
});
app.get('/noticias', (req, res, next) => res.json(noticias));
app.post('/news', (req, res, next) => {
    console.log(req.body);
    noticias.push(req.body.nuevo);
    res.json(noticias);
});

var misFavoritos = [];
app.get('/favs', (req, res, next) => res.json(misFavoritos));
app.post('/favs', (req, res, next) => {
    console.log(req.body);
    misFavoritos.push(req.body.nuevo);
    res.json(misFavoritos);
});
