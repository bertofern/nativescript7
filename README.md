
About me: [bertofern](https://bertofern.wordpress.com/)

### Desarrollo de Aplicaciones Mobile Multiplataforma con Nativescript, Angular y Redux para el Curso "Full Stack Web Development" - Universidad Austral - coursera.org 

# Instación y Uso

## 1. NPM

**Instalar los paquetes**

```
$ npm install
```


## 2. Express Server

**Para arrancar el servidor de la API con Express:**

```
$ cd express-server

$ npm install

$ node app.js
```
_Se interactuará con la API en: http://localhost:3000/_


## 3. NGROK

**Para arrancar el servicio NGROK:**

Aplicativo que sirve para acceder servicios locales de un PC.

Establece lo que se conoce como un túnel de datos, donde el punto de entrada del túnel se establece en una dirección online web de ngrok y el punto de salida del túnel es nuestro puerto local, en nuestra PC. De esa manera nuestro servicio local es accesible desde internet.

Es necesario Registrarse en [NGROK](https://ngrok.com/), descargar y ejecutar **ngrok.exe**. Al registrarse explica bien los pasos a seguir. 

***NOTA: En windows no es necesario "./"***

**Conectarlo con la cuenta a través del token que te brinda NGROK.**
```
./ngrok authtoken 1111111111111_TOKEN_EXAMPLE_1111111111111
```


**Correrlo en el puerto que estamos usando en el servidor express.**
```
./ngrok http 3000
```


**Cojer la ruta https que genera y añadirla al archivo domain/noticias.service.ts en la linea:**
```
appSettings.setString("api", "https://EJEMPLO_DE_URL_GENERADA.ngrok.io");
```


**Problemas con la conectividad de internet en el emulador Android**

Si en el icono de wifi avisa que no hay conección:

Hay que ir a "Extended Controls" del dispositivo "..." Setings -> Proxy -> Manual proxy configuration -> hosting: 8.8.8.8


## 4. Maps SDK for Android

**Activar la API "Maps SDK for Android" de nuestro proyecto en https://console.developers.google.com/**

**Crear una clave de la API en "Credenciales" de nuestro proyecto.**

**Añadir la clave API a *App_Resouces/Android/src/main/res/values/nativescript_google_maps_api.xml* .**

**En la linea:**

```
<string name="nativescript_google_maps_api_key">XXXXX_EXAMPLE_API_KEY_XXXXX</string>
```


## 5. Vista

**Vista de la aplicación en movil IOS o Android**

*Es necesario tener instalado en el mobil "NativeScript Playground" y "NativeScript Preview"*

```
$ tns preview
```


**Vista de la aplicación en un virtual device**

*Es necesario tener instalado Android Studio y SDK de Android. Y tener configurado y corriendo un virtual device en Android Studio para probar la aplicación. Al ejecutarlo entrará en proceso watch/livesync.*

```
$ tns run android
```


## 6. Generar Datos de Prueba

**En la pestaña "Search" clicar en el boton "Generar Datos de Prueba"**

![Generar Datos de Prueba](images/img2.png)


**Datos de Prueba**

```
[
    {"id":1,"titulo":"Futbol Barcelona...","categoria":"Deportes","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":2,"titulo":"Futbol Sevilla...","categoria":"Deportes","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":3,"titulo":"Futbol Lech...","categoria":"Deportes","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":4,"titulo":"Futbol Millan...","categoria":"Deportes","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":5,"titulo":"Polonia...","categoria":"Politica","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":6,"titulo":"España...","categoria":"Politica","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":7,"titulo":"Estados Unidos...","categoria":"Politica","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":8,"titulo":"PIB en España...","categoria":"Economia","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":9,"titulo":"PIB en Polonia...","categoria":"Economia","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false},
    {"id":10,"titulo":"Descubren...","categoria":"Ciencia","nota":"lorem ipsum","countReview":1,"favorita":false,"leyendo":false}
]
```

## 7. Notificaciones con Firebase

**Es necesario registrar la aplicacion en Google Play Console**

https://play.google.com/console/

Como para crear una cuenta se debe de pagar una cuota de 25 $ me salto este paso y tansolo lo dejo la información de ejemplo para una posible puesta en producción de una App real.


**Despues habria que registrar la aplicacion que hay en Google Play Console y añadirla a Firebase**

https://console.firebase.google.com/

Añadir el "Nombre Codigo" de la app de Google Play Console a la nueva app de Firebase.

Descargar el archivo google-services.json y pegarlo en App_Resouces/Android/google-services.json

Para añadir notificaciones ir a -> Crece -> Cloud Messaging -> Nueva notificación


**Añadir plugin Firebase (no necesario, ya existe)**

```
$ tns plugin add nativescript-plugin-firebase
```

**Se creara el firebase.nativescript.json segun las respuestas que le demos al commando anterior (no necesario, ya existe)**

```
    "using_ios": true,
    "using_android": true,
    "analytics": false,
    "firestore": false,
    "realtimedb": false,
    "authentication": false,
    "remote_config": false,
    "performance_monitoring": false,
    "external_push_client_only": false,
    "messaging": true,
    "in_app_messaging": false,
    "crashlytics": false,
    "storage": false,
    "functions": false,
    "facebook_auth": false,
    "google_auth": false,
    "admob": false,
    "dynamic_links": false,
    "ml_kit": false
```

**Añadir las siguientes lineas al archivo: App_Resouces/Android/src/main/AndroidManifest.xml (no necesario, ya existe)**

```
<service android:enabled="false" android:name="com.android.notification.MyFirebaseMessagingService">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT" />
    </intent-filter>
</service>
```

**Añadir las siguientes lineas al archivo: src/app.component.ts (no necesario, ya existe)**

```
    firebase.init({
            onMessageReceivedCallback: (message: Message) => {
                console.log(`título: ${message.title}`);
                console.log(`cuerpo: ${message.body}`);
                console.log(`data: ${JSON.stringify(message.data)}`);
                Toast.makeText("Notificación: " + message.title, "short").show();
            },
            onPushTokenReceivedCallback: (token) => console.log("Firebase push token: " + token)
        }).then(
            () => console.log("firebase.init done"),
            (error) => console.log(`firebase.init error: ${error}`)
        );
```

**Cojer el token del "debug" o del "tns run android" para lanzar notificaciones de prueba**

```
$ debug 
o
$ tns run android
```

**En Firebase añadir el token a la nueva notificación para hacer pruevas en ese dispositivo.**



## 8. Jasmine

**Al arrancar con este comando, crea el archivo de configuración karma.conf.js y instala todas las dependencias (no necesario, ya existe)**
```
$ tns test init
```

**Comando para ver los dispositivos activos.**
```
tns device
```

**Agregar el puerto: 9876 al Firewall. Ir a "Windows Defender Firewall con seguridad avanzada"**
```
-> Reglas de entrada 
-> Nueva regla 
-> Puerto 
-> Puertos locales especificos: 9876 
-> Permitir la conección
-> Nombre: Karma
```

**Correr Jasmine**

```
$ tns build android
$ tns test android
```


---


## Acerca del proyecto

*Prácticas del Módulo 1:*

* Navegar el ecosistema Nativescript.
* Configurar un proyecto para que coexistan las versiones mobile y web.
* Implementar una aplicación móvil basada en una aplicación Angular.

*Puntos:*

1. Que el proyecto está basado en el template https://github.com/NativeScript/template-drawer-navigation-ng. Esto se comprueba con la implementación del enrutador modularizado en features del side drawer.
2. Al menos 2 componentes nuevos.
3. Un nuevo módulo (creado) de features/funcionalidad.
4. Un submódulo específico de ruteo creado para el nuevo módulo.
5. Que la navegación se integra al side drawer preexistente.
6. Un nuevo service de angular (creado) que se use a través de inyección de dependencias a nivel global.
7. Al menos una vista donde se use el ngfor.
8. Al menos un archivo de estilos css que presente sobrecarga de nombres para Android e iOS, es decir, para que se personalicen los estilos en cada sistema operativo.
9. Al menos un ícono que esté personalizado dentro de App_resources, dentro de los recursos de imágenes.
10. Al menos en un componente typescript, un código que asigne un valor a una variable, cuando se está en Android solamente.

---

*Prácticas del Módulo 2:*

* Crear formularios interactivos.
* Mejorar la experiencia del usuario mediante el uso de animaciones.
* Utilizar imágenes y pantallas de bienvenida.

*Puntos:*

1. Una página de búsqueda donde se use un ListView con una plantilla anidada con un FlexboxLayout.
2. Una navegación, desde un listado a una vista de detalle. Ayuda: debe haber una pantalla de listado en la cual, ante un tap sobre un ítem del listado, redirija a la vista de detalle. Debe usarse RouterExtensions y su método "navigate".
3. En al menos un ListView debe usarse el comportamiento del plugin “pull to refresh” para actualizar el listado y agregar elementos nuevos aleatorios o provenientes de un servicio.
4. En uno de los listados, un ícono o botón que muestre al usuario un diálogo de tipo “action” para ingreso de información (por ejemplo, para seleccionar categoría de un elemento) y que esa acción modifique un atributo del objeto subyacente.
5. Un “toast” que notifique algún tipo de información al usuario.
6. La sintáxis “[()]” (“two way binding”, para manejar al menos un formulario de búsqueda o edición.
7. Un validador personalizado, utilizando una directiva de angular (Por ejemplo, puede validarse longitud mínima, email válido, contraseña segura, etc.).
8. Un ícono o botón con detección de gestos, (por ejemplo, double tap o long press).
9. Una animación en algún ícono o botón (por ejemplo, rotate o color).
10. El splash screen que haya sido personalizado para Android.

---

*Prácticas del Módulo 3:*

* Almacenar configuraciones localmente.
* Almacenar datos en base de datos.
* Mantener el estado de la aplicación usando Redux.

*Puntos:*

1. Una aplicación express que exponga un webservice por get y tenga filtrado por querystring.
2. Un listado con un formulario de búsquedas (caja de texto y botón) para filtrado de resultados y el filtrado que funciona.
3. Una variable de configuración donde se pueda configurar la url de Ngrok. Al momento de buscar debe realizarse la conexión desde un Service de la aplicación nativa por http al api en Express como se vió en los videos. 
4. Un service de angular que sea el que realice la solicitud http (para que esa responsabilidad no sea de los componentes gráficos).
5. Una sección de Settings en la app, donde se use AppSettings, LocalStorage o SessionStorage, para leer el nombre de usuario de manera persistente.
6. Una pantalla donde el usuario pueda editar su nombre de usuario y se persista con AppSettings, LocalStorage o SessionStorage.
7. Un ícono para guardar como favorito en el listado de búsqueda,
8. Los favoritos listados en el componente de la sección ‘favoritos’.
9. Un ícono o botón de “Leer ahora” en los favoritos que, usando Redux, despache un action al store de redux.
10. n la pantalla principal, los elementos cliqueados en la acción “Leer ahora” que también se muestran en un listado que se actualizareactivamente al hacer select con la API del Store de Redux.

---

*Prácticas del Módulo 4:*

* Utilizar las capacidades nativas de los dispositivos.
* Segurizar el desarrollo de tu aplicación utilizando pruebas automáticas.
* Automatizar pruebas funcionales.

*Puntos:*

1. El token asignado de firebase que muestra la integración con Google firebase (tus propias claves/keys Google en reemplazo de las de Google), para enviar notificaciones desde la consola de firebase.
2. Un Toast que muestre las notificaciones entrantes a la aplicación.
3. Algún componente donde se utilice el plugin social-share para compartir contenido de tipo texto.
4. Algún componente donde se utilice el plugin social-share para compartir contenido de tipo imagen.
5. Algún componente donde se utilice el plugin 'camera' para tomar fotografías con la cámara del dispositivo. 
6. La imagen de la cámara que puede compartirse por el plugin de social-share.
7. El plugin de mapas de Google, configurado con la cuenta propia.
8. Un marker en el mapa.
9. Una suite de tests de Jasmine que pruebe al menos un reducer de redux.
10. Que se ha configurado la integración a Karma Junit Reporter, verificando que, luego de ejecutar las pruebas unitarias, se genera el archivo de salida del reporter.